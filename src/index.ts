import * as socketio from "socket.io";
import http from "http"

// クライアントとプロジェクトが違うので再宣言
// 線を扱うための型
type Point = {x: number, y:number};
type Line = {from: Point, to: Point};

// いままで描画されて来た線
var allLines : Line[] = [];

// socketIOサーバーを立てるためにはWebサーバーが必要なためhttpモジュールを使って作る。
var server = http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type':'text/html'});
    res.end('server connected');
});

// socketioサーバーを設定するためのオブジェクトを作ってそれに上で作成したwebサーバーを参照させる
const socketIO = socketio.listen(server);
server.listen(8888); // port:8888番をlistenするサーバーを起動

// socketIO.sockets.onで新しい接続があたときのハンドラを登録する
socketIO.sockets.on('connection', socket => {
    
    // クライアントが"emitNewLine"メッセージが送られてきたら、それを全部のクライアントにばらまくようにする
    // newLinesに送られてきた線データが入っている
    socket.on('emitNewLine', (newLines : Line[]) => {
        console.log("emitNewLine")
        socketIO.emit('receiveNewLine', newLines); // ばらまく
        allLines = allLines.concat(newLines); // 送られてきた線を記録する
    });

    // 削除状態を全クライアントに同期するためのハンドラを登録する
    // クライアントから"requestAllDelete"メッセージが送られてきたら、全部のクライアントに"responseAllDelete"を送る。
    socket.on('requestAllDelete', () => {
        allLines = []; // 記録した線をすべて消す
        socketIO.emit('responseAllDelete', {});
    });

    // 新しく接続したクライアントにはいままで記録してきた線を送る
    socket.emit('receiveNewLine', allLines);
    console.log("connected")
});

