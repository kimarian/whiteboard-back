# ホワイトボードのバックエンド

- Hattelで作りそうだったので試しに作って見た。
- 制作時間の都合上プロジェクトをバックとフロントで分けて作ったので、実際に動かすには2つサーバーを起動する必要がある。
- 主要なコードは `src/index.tsx` を参照

## 使用フレームワーク

- React: そこまでまだ必要じゃない。ただ今後の機能の拡張を考えるといるかなと
- socket-io: 描画した線データを参加者全員にばらまくための通信に利用。(ホワイトボードのバックエンドに描いてあるsocketioのサーバーと通信する)

## 動かし方

### 0. npmの用意

npmを使うので https://nodejs.org/en/download/ でインストールすること。
パスとかを通す必要があるかもしれないので適当にやる。

### 1. git clone

以下でgit cloneしてくる。サーバーも必要なので2行とも実行

```shell
$ git clone https://gitlab.com/kimarian/whiteboard-back.git
$ git clone https://gitlab.com/kimarian/whiteboard-front.git
```

### 2. バックエンドのセットアップ、起動

以下をしてバックエンド（socketioのサーバー）を動かす（一行目が重いかもしれない）

```shell
$ cd whiteboard-back
$ npm install
$ npm run start
```

Ctrl-Cとかをすると起動を停止できるが、このあとのコマンドで起動するクライアントが正しく動作しなくなるためしない。このためこのあとのコマンド実行には新しくターミナルを立ち上げる等が必要。

### 3. フロントエンドのセットアップ、起動

以下をしてフロントエンドを動かす。

```shell
$ cd ../whiteboard-front
$ npm install
$ npm run start
```

### 4. ブラウザで `localhost:3000` にアクセス

ブラウザでアドレスバーに `localhost:3000` と打ち込んでにアクセスする。
（もしかしたら自動でブラウザがひらくかもしれない）
